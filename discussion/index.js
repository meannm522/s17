console.log("message from the index.js")
let student1 = 'Martin';
let student2 = 'Maria';
let student3 = 'John';
let student4 = 'Ernie';
let student5 = 'Bert';

// store the ff values inside a single container class.
// to declare an array => we simply use  an "array literal or square bracket []
// use '=' => assignemnt operator to repackage the structure inside the variable
// be careful when selecting variable name
let batch = ['Martin', 'Maria','John', 'Ernie', 'Bert'];
console.log(batch);

// create an array that will contain different computer brands

let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer', 'Toshiba', 'Fujitsu'];

//  ' ' or " " => both are used to declare a string data type in JS
// side lesson

// => How to chose - use case of the data
// 1. if you are going to use qoutations, doalog, inside a string.
console.log(computerBrands);

let message2 = 'Using Javascript\'s Array Literal';
console.log(message2);

// practicing the use of git branches
// ADD this new line of code
// add another message